/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package cat.copernic.jdbc.jdbcexercici2;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Esther
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCExercici2 {

    public static final int ACTION_NONE = 0;
    public static final int ACTION_INSERT = 1;
    public static final int ACTION_UPDATE = 2;
    public static final int ACTION_DELETE = 3;

    public static final int COL_MAT = 1;
    public static final int COL_NOM = 2;
    public static final int COL_DATA_NAIX = 3;

    public static final String CAMP_MAT = "matricula";
    public static final String CAMP_NOM = "nom";
    public static final String CAMP_DATA_NAIX = "data_naixement";

    private static final String SQL_SELECT_ALUMNE = "SELECT matricula, nom, data_naixement FROM alumne WHERE matricula=?";
    private static final String SQL_UPDATE_ALUMNE = "UPDATE alumne SET nom=?, data_naixement=? WHERE matricula=?";
    private static final String SQL_INSERT_ALUMNE = "INSERT INTO alumne (nom, matricula, data_naixement) VALUES (?, ?, ?)";
    private static final String SQL_DELETE_ALUMNE = "DELETE FROM alumne WHERE matricula=?";
    private static final String SQL_SELECT_ALL_ALUMNES = "SELECT matricula, nom, data_naixement FROM alumne";

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
      

        // Crida a la funció per obtenir les dades de connexió
        String[] dadesConnexio = entradaDadesConnexio(sc);

        try (Connection cn = ferConnexio(dadesConnexio[0], dadesConnexio[1], dadesConnexio[2])) {

            // Crida a la funció per obtenir les dades de l'alumne
            String[] dadesAlumne = entradaDadesAlumne(sc);

            String matricula = dadesAlumne[0];
            String nom = dadesAlumne[1];
            LocalDate dataNaixement = LocalDate.parse(dadesAlumne[2], DateTimeFormatter.ofPattern("yyyy-MM-dd"));

            // Converteix el LocalDate a java.sql.Date per inserir-lo a la base de dades
            Date dataNaixSql = Date.valueOf(dataNaixement);

            int action = determinarAccio(cn, sc, matricula, nom, dataNaixSql);

            if (action == ACTION_INSERT) {
                insertarNouAlumne(cn, matricula, nom, dataNaixSql);
            } else if (action == ACTION_UPDATE) {
                modificarAlumne(cn, matricula, nom, dataNaixSql);
            } else if (action == ACTION_DELETE) {
                esborrarAlumne(cn, matricula);
            }

            llistarAlumnes(cn);

        } catch (SQLException ex) {
            Logger.getLogger(JDBCExercici2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static Connection ferConnexio(String url, String user, String password) throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

   private static String[] entradaDadesConnexio(Scanner sc) {
    // Array per emmagatzemar la URL, l'usuari i la contrasenya
    String[] dadesConnexio = new String[3];

    // Demana la URL de connexió
    System.out.print("Introdueix la URL de connexió a la base de dades (per defecte: jdbc:mysql://localhost:3306/fromJava): ");
    dadesConnexio[0] = sc.nextLine();  // Emmagatzema la URL a l'array
    if (dadesConnexio[0].isEmpty()) {
        dadesConnexio[0] = "jdbc:mysql://localhost:3306/fromJava"; // Valor per defecte
    }

    // Demana el nom d'usuari
    System.out.print("Introdueix el nom d'usuari de la base de dades (per defecte: root): ");
    dadesConnexio[1] = sc.nextLine();  // Emmagatzema l'usuari a l'array
    if (dadesConnexio[1].isEmpty()) {
        dadesConnexio[1] = "root"; // Valor per defecte
    }

    // Demana la contrasenya
    System.out.print("Introdueix la contrasenya de la base de dades (per defecte: 123456): ");
    dadesConnexio[2] = sc.nextLine();  // Emmagatzema la contrasenya a l'array
    if (dadesConnexio[2].isEmpty()) {
        dadesConnexio[2] = "123456"; // Valor per defecte
    }

    // Retorna l'array amb la URL, l'usuari i la contrasenya
    return dadesConnexio;
}

    private static String[] entradaDadesAlumne(Scanner sc) {
        System.out.print("Matrícula: ");
        String matricula = sc.nextLine();

        System.out.print("Nom: ");
        String nom = sc.nextLine();

        System.out.print("Data de Naixement (YYYY-MM-DD): ");
        String dataNaixement = sc.nextLine();

        return new String[]{matricula, nom, dataNaixement};
    }

    private static int determinarAccio(Connection cn, Scanner sc, String matricula, String nom, Date dataNaixement) throws SQLException {
        int action = ACTION_INSERT;

        try (PreparedStatement ps = cn.prepareStatement(SQL_SELECT_ALUMNE)) {
            ps.setString(1, matricula);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    System.out.println("Ja existeix un alumne amb aquesta matrícula (" + matricula + ") amb nom: " + rs.getString(COL_NOM)
                            + " i data de naixement: " + rs.getDate(COL_DATA_NAIX));
                    System.out.print("Canviem el nom i/o la data de naixement a: " + nom + ", " + dataNaixement + "? ('S' per continuar): ");
                    if (sc.nextLine().equalsIgnoreCase("S")) {
                        action = ACTION_UPDATE;
                    } else {
                        System.out.print("Vols esborrar aquest alumne? ('S' per continuar): ");
                        if (sc.nextLine().equalsIgnoreCase("S")) {
                            action = ACTION_DELETE;
                        } else {
                            action = ACTION_NONE;
                        }
                    }
                }
            }
        }

        return action;
    }

    private static void insertarNouAlumne(Connection cn, String matricula, String nom, Date dataNaixement) throws SQLException {
        try (PreparedStatement pst = cn.prepareStatement(SQL_INSERT_ALUMNE)) {
            pst.setString(1, nom);
            pst.setString(2, matricula);
            pst.setDate(3, dataNaixement);
            pst.executeUpdate();
            System.out.println("Nou alumne inserit!");
        }
    }

    private static void modificarAlumne(Connection cn, String matricula, String nom, Date dataNaixement) throws SQLException {
        try (PreparedStatement pst = cn.prepareStatement(SQL_UPDATE_ALUMNE)) {
            pst.setString(1, nom);
            pst.setDate(2, dataNaixement);
            pst.setString(3, matricula);
            pst.executeUpdate();
            System.out.println("Dades de l'alumne actualitzades!");
        }
    }

    private static void esborrarAlumne(Connection cn, String matricula) throws SQLException {
        try (PreparedStatement pst = cn.prepareStatement(SQL_DELETE_ALUMNE)) {
            pst.setString(1, matricula);
            pst.executeUpdate();
            System.out.println("Alumne esborrat!");
        }
    }

    private static void llistarAlumnes(Connection cn) {
        try (Statement stmt = cn.createStatement(); ResultSet rs = stmt.executeQuery(SQL_SELECT_ALL_ALUMNES)) {

            System.out.println("\nLlistat de tots els alumnes:");
            Date dataSql;
            while (rs.next()) {
                dataSql = rs.getDate(COL_DATA_NAIX);
                if (dataSql != null) {
                    System.out.println("Matrícula: " + rs.getString(COL_MAT) + ", Nom: " + rs.getString(COL_NOM) + ", Data naixement: " + dataSql.toLocalDate());
                } else {
                    System.out.println("Matrícula: " + rs.getString(COL_MAT) + ", Nom: " + rs.getString(COL_NOM));
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error al llistar els alumnes: " + ex.getMessage());
        }
    }
}
